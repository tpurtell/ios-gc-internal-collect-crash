﻿using System;

using UIKit;
using System.Reactive.Subjects;
using Foundation;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Reactive.Disposables;

namespace Demo1
{
	public partial class WithBehaviorSubjectViewController : UIViewController
	{
		private NSObject[] objects = new NSObject[2000];

		private readonly BehaviorSubject<bool> behaviorSubject = new BehaviorSubject<bool>(false);	
		//NOTE: can not repro with this very similar class. possibly the alignment is just perfect for
		//whatever the Rx version causes to happen
		//private readonly DummySubject<bool> behaviorSubject = new DummySubject<bool>(false);	
		public WithBehaviorSubjectViewController() : base("WithBehaviorSubjectViewController", null)
		{
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);

			Console.WriteLine("Generating memory pressure..."  + GC.CollectionCount(0));
			for (var i = 0; i < this.objects.Length; i++)
			{
				this.objects [i] = new NSObject();
			}
			//automated flip back to root controller
			Task.Run (async () => {
				await Task.Delay(50);
				InvokeOnMainThread(() => {
					NavigationController.PopViewController(false);
				});
			});
		}

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);

			Console.WriteLine("Disposing BehaviorSubject..."  + disposing);
			this.behaviorSubject.Dispose();

			for (var i = 0; i < this.objects.Length; i++)
			{
				this.objects[i].Dispose();
			}
		}
	}
}


