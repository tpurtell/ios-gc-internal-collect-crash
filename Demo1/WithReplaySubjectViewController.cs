﻿using System;

using UIKit;
using System.Reactive.Subjects;
using Foundation;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Reactive.Disposables;

namespace Demo1
{
	public partial class WithReplaySubjectViewController : UIViewController
	{
		private NSObject[] objects = new NSObject[2000];

		private readonly ReplaySubject<bool> behaviorSubject = new ReplaySubject<bool>();	
		public WithReplaySubjectViewController() : base("WithReplaySubjectViewController", null)
		{
		}

		public override void ViewDidAppear(bool animated)
		{
			base.ViewDidAppear(animated);

			Console.WriteLine("Generating memory pressure..."  + GC.CollectionCount(0));
			for (var i = 0; i < this.objects.Length; i++)
			{
				this.objects [i] = new NSObject();
			}
			//automated flip back to root controller
			Task.Run (async () => {
				await Task.Delay(50);
				InvokeOnMainThread(() => {
					NavigationController.PopViewController(false);
				});
			});
		}

		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);

			Console.WriteLine("Disposing ReplaySubject..."  + disposing);
			this.behaviorSubject.Dispose();

			for (var i = 0; i < this.objects.Length; i++)
			{
				this.objects[i].Dispose();
			}
		}
	}
}


