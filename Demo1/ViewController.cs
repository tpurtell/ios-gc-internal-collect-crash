﻿using System;

using UIKit;
using System.Threading.Tasks;

namespace Demo1
{
	public partial class ViewController : UIViewController
	{
		public ViewController (IntPtr handle) : base (handle)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			// Perform any additional setup after loading the view, typically from a nib.

			this.TheButton.TouchUpInside += (sender, e) =>
			{
				this.NavigationController.PushViewController(new WithBehaviorSubjectViewController(), true);
			};

			this.ReplaySubjectButton.TouchUpInside += (sender, e) => 
			{
				this.NavigationController.PushViewController(new WithReplaySubjectViewController(), true);
			};
		}

		public override void ViewWillAppear (bool animated)
		{
			base.ViewWillAppear (animated);
			//automated start of the failing case
			Task.Run (async () => {
				await Task.Delay(50);
				InvokeOnMainThread(() => {
					this.NavigationController.PushViewController(new WithBehaviorSubjectViewController(), false);
					//NOTE: this doesn't cause a crash?!?!  Behavior subject has a lock in the finalizer..
					//this.NavigationController.PushViewController(new WithReplaySubjectViewController(), true);
				});
			});
		}

		//NOTE: if you comment this out, then you get a array type mismatch exception from inside the Task framework
		//if this is in place, you get a native crash!
		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
			Console.WriteLine ("did receive memory warning");
		}
	}
}

