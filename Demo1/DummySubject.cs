﻿using System;

namespace Demo1
{
	public class DummySubject<T> : IDisposable {
		private Exception _exception;
		private bool _isDisposed;
		private T _value;
		private bool _isStopped;
		class Im<T> {
			T[] l;
			public Im() {
				l = new T[0];
			}
		}
		private Im<T> _observers;
		private readonly object _gate = new object ();


		public DummySubject(T t) {
			this._value = t;
			this._observers = new Im<T>();
		}
		public void Dispose() {
			lock (_gate) {
				_isDisposed = false;
				_observers = null;
				_value = default(T);
				_exception = null;
			}
		}
	}
}

