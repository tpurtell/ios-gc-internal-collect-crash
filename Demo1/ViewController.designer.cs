// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Demo1
{
	[Register ("ViewController")]
	partial class ViewController
	{
		[Outlet]
		UIKit.UIButton ReplaySubjectButton { get; set; }

		[Outlet]
		UIKit.UIButton TheButton { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (ReplaySubjectButton != null) {
				ReplaySubjectButton.Dispose ();
				ReplaySubjectButton = null;
			}
			if (TheButton != null) {
				TheButton.Dispose ();
				TheButton = null;
			}
		}
	}
}
